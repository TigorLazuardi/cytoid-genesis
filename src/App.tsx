import AppBar from '@comp/global/AppBar'
import LeftSideBar from '@comp/global/LeftSideBar'
import RightSideBar from '@comp/global/RightSideBar'
import CssBaseline from '@material-ui/core/CssBaseline'
import FlagContainer from '@repo/store/flags'
import UIContainer from '@repo/store/ui'
import React from 'react'

const App = () => {
    return (
        <FlagContainer.Provider>
            <UIContainer.Provider>
                <CssBaseline />
                <div style={{ display: 'flex' }}>
                    <AppBar />
                    <LeftSideBar />
                    <RightSideBar />
                </div>
            </UIContainer.Provider>
        </FlagContainer.Provider>
    )
}

export default App
