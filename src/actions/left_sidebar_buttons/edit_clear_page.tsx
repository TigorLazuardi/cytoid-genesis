import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import EraserIcon from 'mdi-material-ui/EraserVariant'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editClearPageButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => {},
        name: 'Clear Page',
        icon: <EraserIcon />,
        enabled: flags.flags.musicLoaded,
        focused: false,
        tooltip: 'Clears current page from notes',
        keyboard: 'CTRL+D',
    }
}
