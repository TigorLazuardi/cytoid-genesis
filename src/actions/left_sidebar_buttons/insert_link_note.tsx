import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import LinkIcon from 'mdi-material-ui/LinkVariant'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function genLinkNoteButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => {
            flags.toggleInputMode('link-note')
        },
        name: 'Link Note',
        icon: <LinkIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'link-note',
        tooltip: 'Inserts link notes',
        keyboard: '4',
    }
}
