import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import RedoIcon from 'mdi-material-ui/Redo'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editRedoButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => {},
        name: 'Redo',
        icon: <RedoIcon />,
        enabled: flags.flags.musicLoaded,
        focused: false,
        tooltip: 'Redoes undoed action. Redoes are purged upon commit',
        keyboard: 'CTRL+R',
    }
}
