import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import RoadVariantIcon from 'mdi-material-ui/RoadVariant'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function genLongHoldNoteButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('long-hold-note'),
        name: 'Extended Hold Note',
        icon: <RoadVariantIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'long-hold-note',
        tooltip: 'Inserts extended hold notes',
        keyboard: '3',
    }
}
