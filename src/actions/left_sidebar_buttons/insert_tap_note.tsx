import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import RadioButtonChecked from 'mdi-material-ui/RadioboxMarked'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function genTapNote(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('tap-note'),
        name: 'Tap Note',
        icon: <RadioButtonChecked />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'tap-note',
        tooltip: 'Inserts tap / click note.',
        keyboard: '1',
    }
}
