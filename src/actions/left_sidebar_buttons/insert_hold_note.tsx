import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import RoadIcon from 'mdi-material-ui/Road'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function holdNoteButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('hold-note'),
        name: 'Hold Note',
        icon: <RoadIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'hold-note',
        tooltip: 'Inserts hold notes',
        keyboard: '2',
    }
}
