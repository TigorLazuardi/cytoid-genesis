import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import SelectIcon from 'mdi-material-ui/SelectionEllipseArrowInside'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editSelectModeButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('select-mode'),
        name: 'Select Mode',
        icon: <SelectIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'select-mode',
        tooltip: 'Select Notes and gain information about the note. Drag mouse to multi select',
        keyboard: 'Q',
    }
}
