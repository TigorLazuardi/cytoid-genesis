import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import CurveIcon from 'mdi-material-ui/VectorCurve'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editArcDrawButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleFlag('arcDrawing', !flags.flags.arcDrawing),
        name: 'Arc Draw',
        icon: <CurveIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.arcDrawing,
        tooltip:
            'Traces an Arc and insert selected number of notes along the line or everytime the traces passed the Y axis',
        keyboard: 'D',
    }
}
