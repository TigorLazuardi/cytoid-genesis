import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import UndoIcon from 'mdi-material-ui/Undo'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editUndoButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => {},
        name: 'Undo',
        icon: <UndoIcon />,
        enabled: flags.flags.musicLoaded,
        focused: false,
        tooltip: 'Undo previous action. Undo history is purged upon commit',
        keyboard: 'CTRL+Z',
    }
}
