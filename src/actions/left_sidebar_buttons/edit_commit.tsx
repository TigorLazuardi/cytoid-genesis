import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import UpArrowCollapse from 'mdi-material-ui/ArrowCollapseUp'
import React from 'react'

export default function editCommitButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => {},
        name: 'Commit',
        icon: <UpArrowCollapse />,
        enabled: flags.flags.musicLoaded,
        focused: false,
        tooltip: 'Saves current edit and purges Undo steps from application memory',
        keyboard: 'CTRL+S',
    }
}
