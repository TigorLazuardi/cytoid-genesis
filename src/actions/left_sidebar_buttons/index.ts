import editArcDrawButton from '@act/left_sidebar_buttons/edit_arc_draw'
import editClearPageButton from '@act/left_sidebar_buttons/edit_clear_page'
import editCommitButton from '@act/left_sidebar_buttons/edit_commit'
import editDeleteNoteButton from '@act/left_sidebar_buttons/edit_delete_notes'
import editRedoButton from '@act/left_sidebar_buttons/edit_redo'
import editSelectModeButton from '@act/left_sidebar_buttons/edit_select_mode'
import editUndoButton from '@act/left_sidebar_buttons/edit_undo'
import genFlickNoteButton from '@act/left_sidebar_buttons/insert_flick_note'
import holdNoteButton from '@act/left_sidebar_buttons/insert_hold_note'
import genLinkNoteButton from '@act/left_sidebar_buttons/insert_link_note'
import genLongHoldNoteButton from '@act/left_sidebar_buttons/insert_long_hold_note'
import genTapNote from '@act/left_sidebar_buttons/insert_tap_note'
import React from 'react'

export interface SideBarButtons {
    name: string
    icon: React.ReactNode
    action: () => void
    focused: boolean
    enabled: boolean
    tooltip: string
    keyboard?: string
}

// It needs to be wrapped in function because it will be generated in React runtime
export function genInputButtons(): SideBarButtons[] {
    return [genTapNote(), holdNoteButton(), genLongHoldNoteButton(), genLinkNoteButton(), genFlickNoteButton()]
}

export function genIOButtons(): SideBarButtons[] {
    return [
        editCommitButton(),
        editUndoButton(),
        editRedoButton(),
        editSelectModeButton(),
        editArcDrawButton(),
        editDeleteNoteButton(),
        editClearPageButton(),
    ]
}
