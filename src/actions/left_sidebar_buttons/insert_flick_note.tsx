import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import Rhombus from 'mdi-material-ui/Rhombus'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function genFlickNoteButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('flick-note'),
        name: 'Flick Note',
        icon: <Rhombus />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'flick-note',
        tooltip: 'Inserts finger flicking note',
        keyboard: '5',
    }
}
