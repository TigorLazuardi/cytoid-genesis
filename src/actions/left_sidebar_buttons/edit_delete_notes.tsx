import { SideBarButtons } from '@act/left_sidebar_buttons'
import FlagContainer from '@repo/store/flags'
import DeleteIcon from 'mdi-material-ui/Eraser'
import React from 'react'

// It needs to be wrapped in function because it will be generated in React runtime
export default function editDeleteNoteButton(): SideBarButtons {
    const flags = FlagContainer.useContainer()
    return {
        action: () => flags.toggleInputMode('delete-mode'),
        name: 'Delete Note',
        icon: <DeleteIcon />,
        enabled: flags.flags.musicLoaded,
        focused: flags.flags.inputMode === 'delete-mode',
        tooltip: 'Enters delete mode and select note to delete them',
        keyboard: 'E',
    }
}
