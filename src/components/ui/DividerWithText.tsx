import { createStyles, makeStyles } from '@material-ui/core/styles'
import React from 'react'

const useStyles = makeStyles((theme) =>
    createStyles({
        container: {
            display: 'flex',
            alignItems: 'center',
        },
        border: {
            borderBottom: '1px solid lightgray',
            width: '100%',
        },
        content: {
            paddingTop: theme.spacing(0.5),
            paddingBottom: theme.spacing(0.5),
            paddingRight: theme.spacing(1.5),
            paddingLeft: theme.spacing(1.5),
            fontWeight: theme.typography.fontWeightMedium,
            fontSize: theme.typography.fontSize,
            color: 'lightgray',
        },
    }),
)

interface Props extends React.PropsWithChildren<any> {
    style?: React.CSSProperties
}

const DividerWithText = ({ children, style }: Props) => {
    const classes = useStyles()
    return (
        <div className={classes.container} style={style}>
            <div className={classes.border} />
            <span className={classes.content}>{children}</span>
            <div className={classes.border} />
        </div>
    )
}
export default DividerWithText
