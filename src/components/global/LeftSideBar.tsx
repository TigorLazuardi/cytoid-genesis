import { genInputButtons, genIOButtons } from '@act/left_sidebar_buttons'
import DividerWithText from '@comp/ui/DividerWithText'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction'
import ListItemText from '@material-ui/core/ListItemText'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'
import FlagContainer from '@repo/store/flags'
import UIContainer from '@repo/store/ui'
import clsx from 'clsx'
import CheckBoxBlankCircleICon from 'mdi-material-ui/CheckboxBlankCircle'
import ChevronLeftIcon from 'mdi-material-ui/ChevronLeft'
import React from 'react'

function generateStyles(drawerWidth: number) {
    return makeStyles((theme: Theme) =>
        createStyles({
            menuButton: {
                marginRight: theme.spacing(4),
            },
            hide: {
                display: 'none',
            },
            drawer: {
                width: drawerWidth,
                flexShrink: 0,
                whiteSpace: 'nowrap',
            },
            drawerOpen: {
                width: drawerWidth,
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            },
            drawerClose: {
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                }),
                overflowX: 'hidden',
                width: theme.spacing(7) + 1,
                [theme.breakpoints.up('sm')]: {
                    width: theme.spacing(9) + 1,
                },
            },
            toolbar: {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                padding: theme.spacing(1, 0),
                // necessary for content to be below app bar
                ...theme.mixins.toolbar,
            },
            toolBarLeftSpace: {
                margin: theme.spacing(0, 1),
            },
            hideScrollbar: {
                scrollbarWidth: 'none',
            },
            chevron: {
                position: 'absolute',
                left: drawerWidth - 56,
            },
        }),
    )
}

function LeftSideBar() {
    const UIStore = UIContainer.useContainer()
    const flags = FlagContainer.useContainer()

    // Prevent selecting Select Mode or Delete Mode on Arc Drawing
    React.useEffect(() => {
        if (
            (flags.flags.inputMode === 'select-mode' ||
                flags.flags.inputMode === 'null' ||
                flags.flags.inputMode === 'delete-mode') &&
            flags.flags.arcDrawing
        ) {
            flags.toggleInputMode('tap-note')
        }
    }, [flags.flags.arcDrawing])
    const classes = generateStyles(UIStore.LeftDrawer.width)()

    const open = UIStore.LeftDrawer.open

    return (
        <Drawer
            variant="permanent"
            className={clsx(classes.drawer, {
                [classes.drawerOpen]: open,
                [classes.drawerClose]: !open,
            })}
            classes={{
                paper: clsx({
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                    [classes.hideScrollbar]: true,
                }),
            }}
        >
            <div className={classes.toolbar} onClick={UIStore.LeftDrawer.toggle}>
                <Typography variant="h6" align="center">
                    Toolkit
                </Typography>
                <IconButton onClick={UIStore.LeftDrawer.toggle} className={classes.chevron}>
                    <ChevronLeftIcon />
                </IconButton>
            </div>
            {/* <List className={classes.toolbar} onClick={UIStore.LeftDrawer.toggle}>
                <ListItem>
                    <ListItemText>
                        <Typography variant="h5" align="center">
                            Toolkit
                        </Typography>
                    </ListItemText>
                    <ListItemSecondaryAction style={{ position: 'absolute' }}>
                        <IconButton>
                            <ChevronLeftIcon></ChevronLeftIcon>
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
            </List> */}

            <DividerWithText>Notes</DividerWithText>
            <List>
                {genInputButtons().map((button, index) => (
                    <Tooltip
                        key={index}
                        enterDelay={500}
                        enterNextDelay={200}
                        leaveDelay={0}
                        title={<Typography variant="subtitle1">{button.tooltip}</Typography>}
                    >
                        <ListItem
                            alignItems="center"
                            button
                            disabled={!button.enabled}
                            selected={button.focused}
                            onClick={button.action}
                        >
                            <ListItemIcon>{button.icon}</ListItemIcon>
                            <ListItemText primary={button.name} secondary={button.keyboard}></ListItemText>
                            {open && button.focused ? (
                                <ListItemSecondaryAction>
                                    <IconButton disabled edge="end">
                                        <CheckBoxBlankCircleICon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            ) : undefined}
                        </ListItem>
                    </Tooltip>
                ))}
            </List>
            <DividerWithText>Actions</DividerWithText>
            <List>
                {genIOButtons().map((button, index) => (
                    <Tooltip
                        key={index}
                        enterDelay={500}
                        enterNextDelay={200}
                        leaveDelay={0}
                        title={<Typography variant="subtitle1">{button.tooltip}</Typography>}
                    >
                        <ListItem button disabled={!button.enabled} selected={button.focused} onClick={button.action}>
                            <ListItemIcon>{button.icon}</ListItemIcon>
                            <ListItemText primary={button.name} secondary={button.keyboard}></ListItemText>
                            {open && button.focused ? (
                                <ListItemSecondaryAction>
                                    <IconButton disabled edge="end">
                                        <CheckBoxBlankCircleICon />
                                    </IconButton>
                                </ListItemSecondaryAction>
                            ) : null}
                        </ListItem>
                    </Tooltip>
                ))}
            </List>
        </Drawer>
    )
}

export default LeftSideBar
