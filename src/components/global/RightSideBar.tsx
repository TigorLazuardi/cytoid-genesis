import Divider from '@material-ui/core/Divider'
import Drawer from '@material-ui/core/Drawer'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import UIContainer from '@repo/store/ui'
import ChevronRight from 'mdi-material-ui/ChevronRight'
import InboxIcon from 'mdi-material-ui/Inbox'
import MailIcon from 'mdi-material-ui/Mail'
import React from 'react'

function styleFromProps(drawerWidth: number) {
    return makeStyles((theme: Theme) =>
        createStyles({
            drawer: {
                width: drawerWidth,
                flexShrink: 0,
                whiteSpace: 'nowrap',
            },
            drawerOpen: {
                width: drawerWidth,
                transition: theme.transitions.create('width', {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            },
            drawerHeader: {
                display: 'flex',
                alignItems: 'center',
                padding: theme.spacing(0, 1),
                // necessary for content to be below app bar
                ...theme.mixins.toolbar,
                justifyContent: 'flex-start',
            },
            drawerPaper: {
                width: drawerWidth,
            },
            content: {
                flexGrow: 1,
                padding: theme.spacing(3),
            },
        }),
    )
}

function RightSideBar() {
    const UIStore = UIContainer.useContainer()
    const classes = styleFromProps(UIStore.RightDrawer.width)()
    const open = UIStore.RightDrawer.open

    return (
        <Drawer
            variant="persistent"
            anchor="right"
            className={classes.drawer}
            classes={{
                paper: classes.drawerPaper,
            }}
            open={open}
        >
            <div className={classes.drawerHeader} onClick={UIStore.RightDrawer.toggle}>
                <IconButton onClick={UIStore.RightDrawer.toggle}>
                    <ChevronRight />
                </IconButton>
            </div>
            <Divider />
            <List>
                {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
            <Divider />
            <List>
                {['All mail', 'Trash', 'Spam'].map((text, index) => (
                    <ListItem button key={text}>
                        <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                        <ListItemText primary={text} />
                    </ListItem>
                ))}
            </List>
        </Drawer>
    )
}

export default RightSideBar
