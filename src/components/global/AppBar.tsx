import MaterialAppBar from '@material-ui/core/AppBar'
import IconButton from '@material-ui/core/IconButton'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import UIContainer from '@repo/store/ui'
import clsx from 'clsx'
import MenuIcon from 'mdi-material-ui/Menu'
import React from 'react'

function styleFromProps(leftDrawerWidth: number, rightDrawerWidth: number) {
    return makeStyles((theme: Theme) =>
        createStyles({
            title: {
                flexGrow: 1,
            },
            appBar: {
                margin: 0,
                flexGrow: 1,
                zIndex: theme.zIndex.drawer + 1,
                transition: theme.transitions.create(['width', 'margin'], {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.leavingScreen,
                }),
            },
            appBarShiftFromLeft: {
                marginLeft: leftDrawerWidth,
                width: `calc(100% - ${leftDrawerWidth}px)`,
                transition: theme.transitions.create(['width', 'margin'], {
                    easing: theme.transitions.easing.sharp,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            },
            appBarShiftFromRight: {
                marginRight: rightDrawerWidth,
                width: `calc(100% - ${rightDrawerWidth}px)`,
                transition: theme.transitions.create(['width', 'margin'], {
                    easing: theme.transitions.easing.easeOut,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            },
            appBarShiftBothSides: {
                marginRight: rightDrawerWidth,
                marginLeft: leftDrawerWidth,
                width: `calc(100% - ${leftDrawerWidth + rightDrawerWidth}px)`,
                transition: theme.transitions.create(['width', 'margin'], {
                    easing: theme.transitions.easing.easeOut,
                    duration: theme.transitions.duration.enteringScreen,
                }),
            },
            menuButtonLeft: {
                marginRight: 36,
            },
            menuButtonRight: {
                marginLeft: 36,
            },
            hide: {
                display: 'none',
            },
        }),
    )
}

function AppBar() {
    const UIStore = UIContainer.useContainer()
    const classes = styleFromProps(UIStore.LeftDrawer.width, UIStore.RightDrawer.width)()

    const leftDrawerOpen = UIStore.LeftDrawer.open
    const rightDrawerOpen = UIStore.RightDrawer.open

    return (
        <MaterialAppBar
            position="fixed"
            className={clsx(classes.appBar, {
                [classes.appBarShiftFromLeft]: leftDrawerOpen && !rightDrawerOpen,
                [classes.appBarShiftFromRight]: rightDrawerOpen && !leftDrawerOpen,
                [classes.appBarShiftBothSides]: leftDrawerOpen && rightDrawerOpen,
            })}
        >
            <Toolbar>
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    onClick={UIStore.LeftDrawer.toggle}
                    className={clsx(classes.menuButtonLeft, {
                        [classes.hide]: leftDrawerOpen,
                    })}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    {UIStore.AppBar.text}
                </Typography>
                <IconButton
                    edge="end"
                    color="inherit"
                    aria-label="info"
                    onClick={UIStore.RightDrawer.toggle}
                    className={clsx(classes.menuButtonRight, {
                        [classes.hide]: rightDrawerOpen,
                    })}
                >
                    <MenuIcon />
                </IconButton>
            </Toolbar>
        </MaterialAppBar>
    )
}

export default AppBar
