import { useState } from 'react'
import { createContainer } from 'unstated-next'

export interface Flags {
    projectOpened: boolean
    musicLoaded: boolean
    arcDrawing: boolean
    arcDrawingOnGoing: boolean
    edited: boolean
    currentPage: Page
    inputMode: InputModes
}

export interface FlagState {
    flags: Flags
    toggleInputMode: (key: InputModes) => void
    toggleFlag: <T extends keyof Flags>(key: T, b: boolean) => void
    cancelArcDrawing: () => void
}

type InputModes =
    | 'tap-note'
    | 'hold-note'
    | 'long-hold-note'
    | 'link-note'
    | 'flick-note'
    | 'select-mode'
    | 'delete-mode'
    | 'null'

type Page = 'dashboard' | 'settings' | 'export'

const init: Flags = {
    projectOpened: false,
    musicLoaded: true,
    arcDrawing: false,
    arcDrawingOnGoing: false,
    edited: false,
    currentPage: 'dashboard',
    inputMode: 'null',
}

export function useFlags(): FlagState {
    const [flags, setFlags] = useState(init)
    const toggleInputMode = (key: InputModes) => {
        // No select mode or delete mode on arc drawing
        if (flags.arcDrawing && (key === 'select-mode' || key === 'delete-mode' || key === 'null')) return
        // No changing modes while arc drawing
        if (flags.arcDrawingOnGoing) {
            return
        }
        setFlags({
            ...flags,
            inputMode: key,
        })
    }

    const cancelArcDrawing = () => setFlags({ ...flags, arcDrawing: false })

    const toggleFlag = <T extends keyof Flags>(key: T, b: boolean) => {
        switch (key) {
            case 'inputMode':
                throw new Error('inputMode is not allowed to be set via toggleFlag function')
            case 'arcDrawing':
                if (!b && flags.arcDrawingOnGoing) {
                    throw new Error('cannot disable arcDrawing when user is tracing')
                }
                setFlags({
                    ...flags,
                    [key]: b,
                })
                break
            default:
                setFlags({
                    ...flags,
                    [key]: b,
                })
                break
        }
    }
    return { flags, toggleInputMode, toggleFlag, cancelArcDrawing }
}

const FlagContainer = createContainer(useFlags)

export default FlagContainer
