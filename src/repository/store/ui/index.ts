import { AppBarState, useAppBarUIState } from '@repo/store/ui/AppBar'
import { DrawerState, useDrawerState } from '@repo/store/ui/Drawer'
import { createContainer } from 'unstated-next'

export interface IUIContainer {
    LeftDrawer: DrawerState
    AppBar: AppBarState
    RightDrawer: DrawerState
}

function createUIContainerGroup(): IUIContainer {
    // TODO: Implement Config Hook
    const AppBar = useAppBarUIState()
    const LeftDrawer = useDrawerState()
    const RightDrawer = useDrawerState(false)
    return {
        LeftDrawer,
        AppBar,
        RightDrawer,
    }
}

const UIContainer = createContainer(createUIContainerGroup)

export default UIContainer
