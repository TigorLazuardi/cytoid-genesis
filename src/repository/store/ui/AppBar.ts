import { useState } from 'react'

export interface AppBarState {
    text: string
    setText: React.Dispatch<React.SetStateAction<string>>
}

export function useAppBarUIState(title = 'Cytoid Genesis'): AppBarState {
    let [text, setText] = useState(title)
    return { text, setText }
}
