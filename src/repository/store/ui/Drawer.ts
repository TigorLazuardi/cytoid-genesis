import { Dispatch, SetStateAction, useState } from 'react'

export interface DrawerState {
    open: boolean
    width: number
    setWidth: Dispatch<SetStateAction<number>>
    toggle: () => void
}

export function useDrawerState(open = true): DrawerState {
    const [b, setB] = useState(open)
    const [width, setWidth] = useState(300)
    const toggle = () => setB(!b)
    return {
        open: b,
        width,
        toggle: toggle,
        setWidth,
    }
}
